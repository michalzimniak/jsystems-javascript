// Product
// - id
// - name
// - price
// - promotion


var product = {
    id: '',
    name: "",
    price: 0,
    promotion: false,
    reviews: [ { rating: 5 }]
}

product1 = Object.assign({}, product);
product1.name = 'Jabłka'

product2 = Object.assign({}, product);
product2.name = 'Gruszki'

product3 = Object.assign({}, product);
product3.name = 'Placki'

var products = [
 product1,
 product2,
 product3,
];

console.log(products)